CFLAGS=-Wall -g -std=gnu99
LIBS=-lcrypto -lssl
FILES=main.c untrusted.c trusted.c verifier.c rsa.c sign.c util.c sym.c hash.c log.c hmac.c

all: release report

release: ${FILES} sal.h
	gcc ${CFLAGS} -o main ${FILES}  ${LIBS} 2> out.txt

debug: ${FILES} sal.h
	gcc ${CFLAGS} -D DEBUG -o debug ${FILES} ${LIBS} 2> out.txt

report:
	pdflatex report.tex

clean:
	rm main debug *.pem *.txt* *.csr *.crt *.log serial* *~ *.aux *.dvi *.log *.blg *.bbl *.toc *.lof *.lot *.out
