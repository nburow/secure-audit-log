#!/bin/bash

#remove existing keys / ca infrastructure
rm -rf *.pem *.csr *.crt index.* *.req *.crt

#create trusted key pair:
openssl genrsa -out t_private.pem 1024
openssl rsa -in t_private.pem -out t_public.pem -outform PEM -pubout

#create a self-signed certificate for the CA / trusted:
perl -e 'print "\n"x7' | openssl req -new -x509 -days 365 -key t_private.pem -out t_cert.crt -config openssl.cnf

#create user / untrusted keys and certificates:
openssl genrsa -out u_private.pem 1024
openssl rsa -in u_private.pem -out u_public.pem -outform PEM -pubout
perl -e 'print "\n"x9' | openssl req -config openssl.cnf -new -key u_private.pem -out u.req

#Some CA setup stuff:
echo 01 > serial
touch index.txt

#Sign the user / untrusted certificate request:
perl -e 'print "y\n"x2' | openssl ca -config openssl.cnf -out u.crt -infiles u.req
