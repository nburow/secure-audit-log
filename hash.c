#include "sal.h"

//sha 256 hash function.  Takes message of size msg_len, and puts it in digest, returning the size through
//digest_len
void digest_message(unsigned char *message, size_t msg_len, unsigned char **digest, size_t *digest_len)
{
	EVP_MD_CTX *mdctx;

	if((mdctx = EVP_MD_CTX_create()) == NULL)
		handleErrors();

	if(1 != EVP_DigestInit_ex(mdctx, EVP_sha256(), NULL))
		handleErrors();

	if(1 != EVP_DigestUpdate(mdctx, message, msg_len))
		handleErrors();

	*digest_len = EVP_MD_CTX_size(mdctx);
	if((*digest = (unsigned char *)malloc(*digest_len)) == NULL){
		printf("can't allocate buffer for message digest in hash.c\n");
		abort();
	}

	if(1 != EVP_DigestFinal_ex(mdctx, *digest, digest_len))
		handleErrors();

	EVP_MD_CTX_destroy(mdctx);
}

