#include "sal.h"

//sign data using the key in key_file, return the length of the signature via sig_len
unsigned char *hmac(char *key, size_t key_len, unsigned char *data, size_t data_len, size_t *sig_len){
	//sign X0 with your private key
	EVP_MD_CTX *md_ctx = EVP_MD_CTX_create();
	if(md_ctx == NULL)
		handleErrors();

	EVP_PKEY *privKey = EVP_PKEY_new_mac_key(EVP_PKEY_HMAC, NULL, key, key_len);

	if(EVP_DigestInit_ex(md_ctx, EVP_sha256(), NULL) <= 0)
		handleErrors();
	if(EVP_DigestSignInit(md_ctx, NULL, EVP_sha256(), NULL, privKey) <= 0)
		handleErrors();
	
	if(EVP_DigestSignUpdate(md_ctx, data, data_len) <= 0)
		handleErrors();

	//get length of signature
	if(EVP_DigestSignFinal(md_ctx, NULL, sig_len) <= 0)
		handleErrors();
	if(*sig_len <= 0){
		printf("hmac length is invalid\n");
		abort();
	}
	unsigned char *sig = malloc(*sig_len);
	if(!sig){
		printf("malloc error in sign function\n");
		abort();
	}
	size_t check = *sig_len;
	if(EVP_DigestSignFinal(md_ctx, sig, sig_len) <= 0)
		handleErrors();
	
	if(check != *sig_len){
		printf("hey, we didn't sign everything!\n");
		abort();
	}
	//EVP_PKEY_CTX_free(ctx);
	EVP_MD_CTX_destroy(md_ctx);
	EVP_PKEY_free(privKey);

	return sig;
}

//verify: returns 1 if signature checks, 0 otherwise.
int verify_hmac(char *key, size_t key_len, unsigned char *data, size_t data_len, 
								unsigned char *sig, size_t sig_len){

	size_t cur_sig_len = 0;
	unsigned char *cur_sig = hmac(key, key_len, data, data_len,&cur_sig_len);
	size_t m = (cur_sig_len < sig_len) ? cur_sig_len : sig_len;

	return !memcmp(sig, cur_sig, m); 
}
