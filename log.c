#include "sal.h"

unsigned char *a = NULL;
unsigned char *y = NULL;

static int log_num = 0;

int read_len(char **file, int *a, int *b, int *c, int *d, int *e){
	char *f = *file;
	int count = 0;

	for(int i = 0; i < 5; i++){
		while(*f != ','){
			switch (i){
				case 0:
					*a *= 10;
					*a += *f - '0';
					break;
				case 1:
					*b *= 10;
					*b += *f - '0';
					break;
				case 2:
					*c *= 10;
					*c += *f - '0';
					break;
				case 3:
					*d *= 10;
					*d += *f - '0';
					break;
				case 4:
					*e *= 10;
					*e += *f - '0';
					break;
			}
			//next char for while
			f++;
			count++;
		}
		//skip the comma
		f++;
		count++;
	}

	*file = f;
	return count;
}

//returns w, d, k, y, and z from the log file file.
int log_getnextentry(char **f, unsigned char **w, unsigned char **iv, unsigned char **d,
					unsigned char **y, unsigned char **z, int *w_len, int *iv_len, int *cipher_size,
					int *y_len, int *z_len){
	char *file = *f;
	if(strstr(file, "START LOG ENTRY: ") == NULL){
		printf("Error parsing log file to verify hash: didn't find start of entry\n");
		abort();
	}
	file += strlen("START LOG ENTRY: ");
	//read += strlen("START LOG ENTRY: ");

	//skip the log entry number
	while(*file != ')') {file++;} // read++;}
	file++; //skip the )
	//read++;

	//int w_len = 0, iv_len = 0, cipher_size = 0, y_len = 0, z_len = 0;
	read_len(&file, w_len, iv_len, cipher_size, y_len, z_len);
	
	if(*y_len != HASH_LEN || *z_len !=HASH_LEN){
		printf("failed size sanity check in getnext_entry\n");
		abort();
	}
	*w = malloc(*w_len);
	memcpy(*w, file, *w_len);
	file += *w_len;

	*iv = malloc(*iv_len);
	memcpy(*iv, file, *iv_len);
	file += *iv_len;

	*d	= malloc(*cipher_size);
	memcpy(*d, file, *cipher_size);
	file += *cipher_size;

	*y = malloc(*y_len);
	memcpy(*y, file, *y_len);
	file += *y_len;

	*z = malloc(*z_len);
	memcpy(*z, file, *z_len);
	file += *z_len;

	int read = file - *f;
	*f = file;
	return read;
}

void get_next_a(unsigned char **a){
	unsigned char *a2 = *a;
	//increment a2
	char *msg = "Increment Hash";
	size_t concat_len = strlen(msg)+1 + HASH_LEN;
	unsigned char *concat = malloc(concat_len);
	memcpy(concat, msg, strlen(msg)+1);
	unsigned char *tmp = concat + strlen(msg) + 1;
	memcpy(tmp, a2, HASH_LEN);
	free(a2);
	a2 = NULL;
	size_t a_len;
	digest_message(concat, concat_len, &a2, &a_len);
	if(a_len != HASH_LEN){
		printf("incrementing a for log verification went wrong\n");
		abort();
	}
	free(concat);

	*a = a2;
}

//verifies the hash chain, and hmac's
int log_verifyhash(char *file, int len, unsigned char *A){
	unsigned char *wread = NULL;
	unsigned char *ivread = NULL;
	unsigned char *dread = NULL;
	
	unsigned char *ycalc = malloc(HASH_LEN);
	unsigned char *yread = NULL;
	memset(ycalc, '\0', HASH_LEN);

	unsigned char *zread = NULL;
	unsigned char *zcalc = NULL;
	size_t zcalc_len = 0;

	int check_z = 0;
	unsigned char *a2;
	if(A != NULL){
		a2 = malloc(HASH_LEN);
		memcpy(a2, A, HASH_LEN);
		check_z = 1;
	}

	size_t concat_len = 0;
	unsigned char *concat;
	unsigned char *tmp;

	int read = 0;
	int wl = 0, ivl = 0, dl = 0, yl = 0, zl = 0;

	dprintf("verifying log hash and signature chain\n");
	//parse the log entry by entry
	//account for EOF which we don't read in the loop bound
	while(read < len - 1){   
		read += log_getnextentry(&file, &wread, &ivread, &dread, &yread, &zread,
								&wl, &ivl, &dl, &yl, &zl);

		if(!strcmp("AbnormalCloseType", wread)) return 0;

		concat_len = HASH_LEN + dl + wl;
		if((concat = malloc(concat_len)) == NULL){
			printf("Malloc failed in log_verifyhash\n");
			abort();
		}
		memcpy(concat, ycalc, HASH_LEN);
		tmp = concat + HASH_LEN;
		memcpy(tmp, wread, wl);
		tmp += wl;
		memcpy(tmp, dread, dl);

		free(ycalc);
		ycalc = NULL;
		size_t ycalc_len;
		digest_message(concat, concat_len, &ycalc, &ycalc_len);
		if(ycalc_len != HASH_LEN){
			printf("problem with hash function in verifyhash\n");
			abort();
		}
		#if DEBUG
     		printf("concat_len 2: %d\n", concat_len);
			FILE *off = fopen("verify.txt", "wb");
     		fwrite(concat, sizeof(char), concat_len, off);
     		fclose(off); 
     		off = fopen("ycurr.txt", "wb");
     		fwrite(ycalc, sizeof(char), ycalc_len, off);
     		fclose(off); 
			off = fopen("yread.txt", "wb");
			fwrite(yread, sizeof(char), yl, off);
			fclose(off);
    	#endif
		
		free(concat);
	
		if(check_z){
			//calculate z:
			zcalc = hmac(a2, HASH_LEN, ycalc, ycalc_len, &zcalc_len);
			if(zcalc_len != HASH_LEN){
				printf("computing z for verification failed\n");
				abort();
			}
			if(memcmp(zread, zcalc, HASH_LEN)) return 0;
			free(zcalc);
			zcalc = NULL;
		
			get_next_a(&a2);
		}

		//if they don't match return 0
		if(memcmp(yread, ycalc, HASH_LEN)) return 0;

		dprintf("verified a hash chain entry\n");
		dprintf("read %d len %d\n", read, len);

		wl = 0;
		ivl = 0;
		dl = 0;
		yl = 0;
		zl = 0;

		free(wread);
		free(ivread);
		free(dread);
		free(yread);
		free(zread);
		wread = ivread = dread = yread = zread = NULL;
	}
	dprintf("verified the hash chain!\n");
	return 1;
}

size_t log_getmsg(unsigned char *file, int len, unsigned char *A, unsigned char **ret,
									unsigned char **key, unsigned char **iv_ret){
	static unsigned char *f = NULL;
	static int read = 0;
	static unsigned char *a = NULL;

	dprintf("in log_getmsg\n");
	//set up for a new sequence of reads
	if(file != NULL) {
		f = file;
		read = 0;
		free(a);
		a = malloc(HASH_LEN);
		memcpy(a, A, HASH_LEN);
		dprintf("starting from beginning of log file\n");
	}
		
	//reached the end of a file
	if(read >= len - 1) {
		*ret = NULL;
		return 0;
	}
	
	unsigned char *wread = NULL;
	unsigned char *ivread = NULL;
	unsigned char *dread = NULL;
	unsigned char *yread = NULL;
	unsigned char *zread = NULL;

	int wl = 0, ivl = 0, dl = 0, yl = 0, zl = 0;
	
	read += log_getnextentry(&f, &wread, &ivread, &dread, &yread, &zread,
													 &wl, &ivl, &dl, &yl, &zl);

	while(read < len - 1 && (!strcmp("LogfileInitizlizationType", wread) ||
													 !strcmp("ResponseMessageType", wread))){
		wl = 0;
		ivl = 0;
		dl = 0;
		yl = 0;
		zl = 0;
		free(wread);
		free(ivread);
		free(dread);
		free(yread);
		free(zread);
		wread = ivread = dread = yread = zread = NULL;
		read += log_getnextentry(&f, &wread, &ivread, &dread, &yread, &zread,
													 &wl, &ivl, &dl, &yl, &zl);
		get_next_a(&a);
	}
	if(!strcmp("AbnormalCloseType", wread) || !strcmp("NormalCloseMessage", wread)){
		*ret = NULL;
		return 0;
	}
	dprintf("valid message type\n");

	unsigned char *msg = NULL;
	size_t key_len;

	//calculate the key
	unsigned char *concat, *tmp;
	size_t concat_len = strlen("Encryption Key") + 1 + wl + HASH_LEN;
	concat = malloc(concat_len);

	memcpy(concat, "Encryption Key", strlen("Encryption Key") + 1);
	tmp = concat + strlen("Encryption Key") + 1;
	memcpy(tmp, wread, wl);
	tmp += wl;
	memcpy(tmp, a, HASH_LEN);

	unsigned char *tmp_key;
	if(key)
		digest_message(concat, concat_len, key, &key_len); 
	else
		digest_message(concat, concat_len, &tmp_key, &key_len);

	//decrypt the message
	msg = malloc(concat_len);
	size_t msg_len = dl;
	if(key)
		*ret = dread;
	else{
		msg_len = decrypt_aes(dread, dl, tmp_key, ivread, msg);
		*ret = msg;
		free(dread);
	}

	if(iv_ret)
		*iv_ret = ivread;
	else
		free(ivread);
	
	free(wread);
	free(yread);
	free(zread);

	dprintf("decrypted the message, returning it\n");

	//increment a
	get_next_a(&a);

	return msg_len;
}

void log_close(FILE *fp){
	time_t d;
	time(&d);
	log_message(fp, "NormalCloseMessage", &d, sizeof(time_t), NULL);
	RAND_bytes(a, HASH_LEN);
	free(y);
	//K is already destroyed by log_message, kept on the stack frame, lost on return
	//well, lost enough for us anyway ;)
}
/*Logs a message per the protocol */
//A is passed in the first time, otherwise null to used
//saved value
//w must be a string
//d can be an arbitrary byte pattern
void log_message(FILE *fp, char *w, char *d, size_t d_len, unsigned char *A){
	unsigned char *k = NULL;
	unsigned char iv[KEY_LEN];
	unsigned char *ek_d;
	unsigned char *z;
	unsigned char *tmp;
	unsigned char *msg = "Encryption Key";
	unsigned char *msg2 = "Increment Hash";

	dprintf("about to write a log message, exciting!\n");

	if(A){
		dprintf("its the start of a new log!\n");
		free(a);
		a = malloc(HASH_LEN);
		memcpy(a, A, HASH_LEN);
		free(y);
		y = malloc(HASH_LEN);
		memset(y, '\0', HASH_LEN);
		log_num = 0;
	}
	else
		dprintf("adding a new entry to an existing log\n");

	//compute k
	size_t concat_len = strlen(msg) + 1 + strlen(w) + 1 + HASH_LEN;
	unsigned char *concat = malloc(concat_len);
	memcpy(concat, msg, strlen(msg) + 1);
	tmp = concat + strlen(msg) + 1;
	memcpy(tmp, w, strlen(w) + 1);
	tmp += strlen(w) + 1;
	memcpy(tmp, a, HASH_LEN);

	size_t k_len;
	digest_message(concat, concat_len, &k, &k_len);
	if(k_len != HASH_LEN){
		printf("comuting k went sideways on me\n");
		abort();
	}
	free(concat);
	
	//encrypt d and add it to the log entry
	RAND_bytes(iv, KEY_LEN);
	unsigned char *ciphertext = malloc((d_len + 16) & ~15);
	size_t cipher_size = encrypt_aes(d, d_len, k, iv, ciphertext);
	free(k);

	//calculate y and add it to the log entry
	concat_len = HASH_LEN + cipher_size + strlen(w) + 1;
	concat = malloc(concat_len);
	memset(concat, '\0', concat_len);
	memcpy(concat, y, HASH_LEN);
	tmp = concat + HASH_LEN;
	memcpy(tmp, w, strlen(w) + 1);
	tmp += strlen(w) + 1;
	memcpy(tmp, ciphertext, cipher_size);
	free(y);
	y = NULL;

	size_t y_len;
	digest_message(concat, concat_len, &y, &y_len);
	if(y_len != HASH_LEN){
		printf("something hinky in computing hash for Yj\n");
		abort();
	}
	//write it to the log
	#if DEBUG
     if(log_num == 0){
		 	printf("concat_len1: %d\n", concat_len);
			FILE *off = fopen("pre2.txt", "wb");
     	fwrite(y, sizeof(char), y_len, off);
     	fclose(off); 
		 	off = fopen("pre.txt", "wb");
     	fwrite(concat, sizeof(char), concat_len, off);
     	fclose(off); 
			unsigned char *foo = NULL;
			size_t foo_l;
			digest_message(concat, concat_len, &foo, &foo_l);
			if(memcmp(foo, y, foo_l)) printf("can't verify the hash immediately afterwards\n");
			free(foo);
		}
    #endif


    free(concat);
	
	//calculate z and add it to the log entry
	size_t z_len = 0;
	z = hmac(a, HASH_LEN, y, y_len, &z_len);
	if(z_len != HASH_LEN){
		printf("something went wrong when computing z hmac to store \n");
		abort();
	}
	#if DEBUG
	if(!verify_hmac(a, HASH_LEN, y, y_len, z, z_len)){
		printf("failed to immediately verify hmac, code must be buggy!\n");
		abort();
	}
	#endif

	//increment A
	concat_len = strlen(msg2) + 1 + HASH_LEN;
	concat = malloc(concat_len);
	memcpy(concat, msg2, strlen(msg2) + 1);
	tmp = concat + strlen(msg2) + 1;
	memcpy(tmp, a, HASH_LEN);
	free(a);
	a = NULL;
	size_t a_len;
	digest_message(concat, concat_len, &a, &a_len);
	if(a_len != HASH_LEN){
		printf("danger master robinson, calculating Aj+1 went amiss amuck and awry!\n");
		abort();
	}
	free(concat);

	//write the entry number and the w field
	fprintf(fp, "START LOG ENTRY: %d)%d,%d,%d,%d,%d,", log_num, strlen(w)+1, KEY_LEN,
																											cipher_size, y_len, z_len);
	fwrite(w, sizeof(char), strlen(w)+1, fp);
	fwrite(iv, sizeof(char), KEY_LEN, fp);
	fwrite(ciphertext, sizeof(char), cipher_size, fp);
	free(ciphertext);

	fwrite(y, sizeof(char), y_len, fp);
	
	fwrite(z, sizeof(char), z_len, fp);
	free(z);

	//write a newline to the log
	fprintf(fp, "\n");	

	log_num++;
}
