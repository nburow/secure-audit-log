#include "sal.h"
#define CMDSIZE 256

void handleErrors(void){
	ERR_print_errors_fp(stderr);
	abort();
}

int main(int argc, char **argv){
	unsigned char buf[CMDSIZE];
	unsigned char *cmd = NULL;
	unsigned char *cmd_arg = NULL;
	int len = 0;

	memset(buf, '\0', CMDSIZE);

	//set up OPENSSL
	SSL_library_init();
	ERR_load_crypto_strings();
	OpenSSL_add_all_algorithms();
	OPENSSL_config(NULL);

	FILE *fp = NULL;
	unsigned char log_id[256];
	memset(log_id, '\0', 256);
	int log_num = 2;
	while(1){
		//get the next line from STDIN. 
		//TODO: make sure we can have a length assumption
		fgets((char *)buf, CMDSIZE, stdin);
		
		//remove the trailing \n
		len = strlen((const char*)buf);
		buf[len-1] = '\0';
		len--;

		cmd = buf;

		//trim any leading whitespace
		while(isspace(*cmd)) cmd++; 

		//check the valid commands, must be at beginning of string
		if((cmd_arg = (unsigned char *)strstr((const char *)cmd, "newlog")) != NULL 
				&& cmd_arg == cmd){
			if(fp != NULL){
				printf("Invalid command, a log is already open.  Close it first.\n");
				continue;
			}
			len = strlen("newlog");
			cmd_arg += len + 1;
			cmd[len] = '\0';
			if ( strlen((char *)cmd_arg) <= 0){
				printf("you must specify a file name after newlog\n");
				continue;
			}
			strcpy(log_id, cmd_arg);
			fp = fopen(cmd_arg, "w");
			if(!u_makelog(cmd_arg, fp)){
				fprintf(stderr, "Make Log failed\n");
				abort();
			}
			printf("Created log %s\n", cmd_arg);
		}
		else if((cmd_arg = strstr((const char *)cmd, "append")) != NULL && cmd_arg == cmd){
			if(fp == NULL) {
				printf("No log open\n");
				continue;
			}
			len = strlen("append");
			cmd_arg += len + 1;
			cmd[len] = '\0';
			log_message(fp, "LogEntryType", cmd_arg, strlen(cmd_arg)+1, NULL);
			printf("Added Log Entry number %d\n", log_num);
			log_num++;
		}
		//note: close log doesn't take any arguments
		else if((cmd_arg = strstr((const char *)cmd, "closelog")) != NULL && cmd_arg == cmd){
			if(fp == NULL) {
				printf("No log open\n");
				continue;
			}
			len = strlen("closelog");
			cmd_arg += len + 1;
			cmd[len] = '\0';
			u_closelog(fp);
			fclose(fp);
			fp = NULL;
			log_num = 2;
		}
		else if((cmd_arg = strstr((const char *)cmd, "verifylog")) != NULL && cmd_arg == cmd){
			char *tmp = strtok(cmd, " ");
			char *log = strtok(NULL, " ");
			char *out = strtok(NULL, " ");
			if(!tmp || !log || !out){
				printf("Invalid use of verifylog, which is odd from the person who specified its behaviour!\n");
				continue;
			}
			//log is the current log
			//printf("log to verify: %s\nCurrent log: %s\n", log, log_id);
			if(!strcmp(log_id, log)){
				printf("can't verifylog the current log\n");
				continue;
			}
			FILE *fp_tmp = fopen(log, "rb");
			if(fp_tmp == NULL){
				printf("no such log to verify\n");
				continue;
			}
			if(!t_verifylog(fp_tmp, out, log))
				printf("Failed verification\n");
			fclose(fp_tmp);
			fp_tmp = NULL;
		}
		else if((cmd_arg = strstr((const char *)cmd, "verify")) != NULL && cmd_arg == cmd){
			len = strlen("verify");
			cmd_arg += len + 1;
			cmd[len] = '\0';
			int entry_no = atoi(cmd_arg);
			if(!fp){
				printf("Log file must be open to issue verify command\n");
				continue;
			}
			if(entry_no < 2 || entry_no >= log_num ){
				printf("Invalid entry number\n");
				continue;
			}
			entry_no -= 2;

			if(!v_entry(fp, log_id, entry_no))
				printf("Failed verification\n");
		}
		else if((cmd_arg = strstr((const char *)cmd, "exit")) != NULL && cmd_arg == cmd){
			dprintf("So long and thanks for all the fish!\n");
			break;
		}
		dprintf("%s %s\n", cmd, cmd_arg);
	}

	CRYPTO_cleanup_all_ex_data();
	ERR_free_strings();
	ERR_remove_thread_state(0);
	EVP_cleanup();
	
	return EXIT_SUCCESS;
}
