#include "sal.h"

//encrypt data with the public key in key_file
unsigned char *encrypt_rsa(char *key_file, unsigned char *data, size_t data_len, size_t *outlen){
	EVP_PKEY *key = read_key(1, key_file);
	EVP_PKEY_CTX *ctx;
	unsigned char *out;
	size_t check;

	//set up and initialization
	if((ctx = EVP_PKEY_CTX_new(key, NULL)) == NULL)
		handleErrors();
	if(EVP_PKEY_encrypt_init(ctx) <= 0)
		handleErrors();
	if(EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_OAEP_PADDING) <= 0)
		handleErrors();

	//find output length:
	if(EVP_PKEY_encrypt(ctx, NULL, outlen, data, data_len) <= 0)
		handleErrors();

	//the actual encryption
	check = *outlen;
	out = malloc(*outlen);
	if(!out){
		printf("malloc failed to create cipher buffer in RSA encrypt\n");
		abort();
	}
	if(EVP_PKEY_encrypt(ctx, out, outlen, data, data_len) <= 0)
		handleErrors();

	//sanity check
	if(*outlen != check){
		printf("hey, we didn't encrypt everypting - RSA\n");
		abort();
	}

	//clean up
	EVP_PKEY_free(key);
	EVP_PKEY_CTX_free(ctx);

	return out;
}

//decrypt
unsigned char *decrypt_rsa(char *key_file, unsigned char *data, size_t data_len, size_t *outlen){
	EVP_PKEY *key = read_key(0, key_file);
	EVP_PKEY_CTX *ctx;
	unsigned char *out;

	//set up and initialization
	if((ctx = EVP_PKEY_CTX_new(key, NULL)) == NULL)
		handleErrors();
	if(EVP_PKEY_decrypt_init(ctx) <= 0)
		handleErrors();
	if(EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_OAEP_PADDING) <= 0)
		handleErrors();

	//find output length:
	if(EVP_PKEY_decrypt(ctx, NULL, outlen, data, data_len) <= 0)
		handleErrors();

	//the actual encryption
	out = malloc(*outlen);
	if(!out){
		printf("malloc failed to create cipher buffer in RSA encrypt\n");
		abort();
	}
	if(EVP_PKEY_decrypt(ctx, out, outlen, data, data_len) <= 0)
		handleErrors();

	//clean up
	EVP_PKEY_free(key);
	EVP_PKEY_CTX_free(ctx);

	return out;
}
