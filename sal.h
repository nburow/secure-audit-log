#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <time.h>

//openssl headers:
#include<openssl/conf.h>
#include<openssl/evp.h>
#include<openssl/err.h>
#include<openssl/rand.h>
#include<openssl/ssl.h>
#include<openssl/rsa.h>
#include<openssl/pem.h>
#include<openssl/engine.h>

#ifdef DEBUG
#define dprintf(...) do { printf(__VA_ARGS__); } while(0)
#else
#define dprintf(...)
#endif

//Constants:

//16 bytes = 128 bit; 32 bytes = 256 bits
#define KEY_LEN 16
#define HASH_LEN 32
#define TIMEOUT 10     //how long until timeout in setup protocol

//Function Prototypes

/*In main.c */
void handleErrors(void);

/*In untrusted.c */
int u_makelog(unsigned char *name, FILE *fp);
void u_logallowed(int m1_components, size_t m1_size, size_t *m1_sizes, unsigned char *m1,
				int concat_components, size_t concat_size, size_t *concat_sizes,
				int x1_components, size_t x1_size, size_t *x1_sizes,
				unsigned char *iv);
void u_closelog(FILE *fp);

/*In trusted.c */
void t_allowlog(int m0_components, size_t m0_size, size_t *m0_sizes, unsigned char *m0,
				int concat_components, size_t concat_size, size_t *concat_sizes,
				int x0_components, size_t x0_size, size_t *x0_sizes,
				unsigned char *iv);
void t_closelog(void);
int t_verifylog(FILE *, char *, char *);
int t_verifyentry(int, unsigned char *, int, unsigned char **, unsigned char **,
															unsigned char **, size_t *);

/* In verifier.c */
int v_entry(FILE *, char *, int);

/*In util.c*/
EVP_PKEY *read_key(int pub, char *file);
unsigned char *read_cert(size_t *cert_len, char *cert_name);

//reads a certificate into an OpenSSL X509 struct
X509 *read_x509(char *file);

//concatenates x and its signature
unsigned char *make_concat(unsigned char *x, unsigned char *sig, size_t concat_size, size_t *concat_sizes);

//glues x0 / x1 together, and returns metadata via x0_size, x0_sizes
unsigned char *make_x(int *p, time_t *d, unsigned char *cert, size_t *x_size, 
						size_t *x_sizes, size_t cert_len, unsigned char *A);

//stitch m together
unsigned char *make_m(unsigned char *encryp_key, unsigned char *ciphertext, size_t *m_sizes, size_t m_size, int p, char *log_id);

unsigned char *process_message(unsigned char **sig_p, size_t m_size, size_t *m_sizes,
                               unsigned char *m, size_t concat_size, size_t *concat_sizes,
                               size_t x_size, unsigned char *iv, unsigned char **id, int p,
                               char *priv_key, unsigned char *key);


/*In rsa.c */
unsigned char *encrypt_rsa(char *key_file, unsigned char *data, size_t data_len, size_t *outlen);
unsigned char *decrypt_rsa(char *key_file, unsigned char *data, size_t data_len, size_t *outlen);

/*In sign.c */
unsigned char *sign(char *key_file, unsigned char *data, size_t data_len, size_t *sig_len);
int verify(char *key_file, unsigned char *data, size_t data_len, unsigned char *sig, size_t sig_len);

/*In sym.c */
size_t encrypt_aes(unsigned char *plaintext, size_t plaintext_len, unsigned char *key,
  unsigned char *iv, unsigned char *ciphertext);

size_t decrypt_aes(unsigned char *ciphertext, size_t ciphertext_len, unsigned char *key,
  unsigned char *iv, unsigned char *plaintext);

/*In hash.c */
void digest_message(unsigned char *message, size_t msg_len, unsigned char **digest, size_t *digest_len);

/* In hmac.c */
unsigned char *hmac(char *key, size_t key_len, unsigned char *data, size_t data_len, size_t *sig_len);
int verify_hmac(char *key, size_t key_len, unsigned char *data, size_t data_len, 
								unsigned char *sig, size_t sig_len);

/* In log.c */
void log_message(FILE *fp, char *w, char *d, size_t d_len, unsigned char *A);
void log_close(FILE *fp);
int log_verifyhash(char *, int, unsigned char *);
size_t log_getmsg(unsigned char *, int, unsigned char *, unsigned char **, 
									unsigned char**, unsigned char **);
