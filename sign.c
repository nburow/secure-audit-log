#include "sal.h"

//sign data using the key in key_file, return the length of the signature via sig_len
unsigned char *sign(char *key_file, unsigned char *data, size_t data_len, size_t *sig_len){
	//sign X0 with your private key
	EVP_MD_CTX *md_ctx = EVP_MD_CTX_create();
	EVP_PKEY_CTX *ctx;
	EVP_PKEY *privKey = read_key(0, key_file);

	if((ctx = EVP_PKEY_CTX_new(privKey, NULL)) == NULL) 
		handleErrors();
	if(EVP_DigestSignInit(md_ctx, &ctx, EVP_sha256(), NULL, privKey) <= 0)
		handleErrors();
	if(EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_PADDING) <= 0)
		handleErrors();
	
	if(EVP_DigestSignUpdate(md_ctx, data, data_len) <= 0)
		handleErrors();

	//get length of signature
	if(EVP_DigestSignFinal(md_ctx, NULL, sig_len) <= 0)
		handleErrors();
	unsigned char *sig = malloc(*sig_len);
	if(!sig){
		printf("malloc error in sign function\n");
		abort();
	}
	size_t check = *sig_len;
	if(EVP_DigestSignFinal(md_ctx, sig, sig_len) <= 0)
		handleErrors();
	
	if(check != *sig_len){
		printf("hey, we didn't sign everything!\n");
		abort();
	}
	//EVP_PKEY_CTX_free(ctx);
	EVP_MD_CTX_destroy(md_ctx);
	EVP_PKEY_free(privKey);

	return sig;
}

//verify: returns 1 if signature checks, 0 otherwise.
int verify(char *key_file, unsigned char *data, size_t data_len, unsigned char *sig, size_t sig_len){
	int ans = 0;

	//sign X0 with your private key
	EVP_MD_CTX *md_ctx = EVP_MD_CTX_create();
	EVP_PKEY_CTX *ctx;
	EVP_PKEY *pubKey = read_key(1, key_file);
	
	if((ctx = EVP_PKEY_CTX_new(pubKey, NULL)) == NULL)
		handleErrors();
	if(EVP_DigestVerifyInit(md_ctx, &ctx, EVP_sha256(), NULL, pubKey) <= 0)
		handleErrors();
	if(EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_PADDING) <= 0)
		handleErrors();
	
	if(EVP_DigestVerifyUpdate(md_ctx, data, data_len) <= 0)
		handleErrors();

	//strict bound.  0 only indicates signature failed to verify, not error
	if((ans = EVP_DigestVerifyFinal(md_ctx, sig, sig_len)) < 0)
		handleErrors();
	
	//EVP_PKEY_CTX_free(ctx);
	EVP_MD_CTX_destroy(md_ctx);
	EVP_PKEY_free(pubKey);

	return ans;
}
