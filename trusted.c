#include "sal.h"

typedef struct {
	//symtetric key used for messages from Untrusted
	 unsigned char ukey[KEY_LEN];
	 unsigned char u_iv[KEY_LEN];

	//symetric key used to send messages to Untrusted
	 unsigned char tkey[KEY_LEN];
	 unsigned char t_iv[KEY_LEN];

	 unsigned char *A[HASH_LEN];

	 int p;
	 unsigned char *log_id;
} Entry;

static Entry logs[200];
static int num = -1;

int t_verifyentry(int e_no, unsigned char *file, int len, unsigned char **entry, 
									unsigned char **key, unsigned char **iv_ret, size_t *l){
	if(!log_verifyhash(file, len, logs[num].A)){
		return 0;
	}
	*l = log_getmsg(file, len, logs[num].A, entry, key, iv_ret);
	e_no--;
	while(e_no >= 0 && *entry != NULL){
		free(*entry);
		free(*key);
		free(*iv_ret);
		*l = log_getmsg(NULL, len, NULL, entry, key, iv_ret);
		e_no--;
	}
	return entry != NULL ? 1 : 0;
}

int t_verifylog(FILE *fp, char *out, char *log_name){
	//find size of the log file
	fseek(fp, 0, SEEK_END);
	int len = ftell(fp);
	rewind(fp);

	int log_num = 0;
	while(log_num <= num && strcmp(log_name, logs[log_num].log_id)) log_num++;

	if(!(log_num <= num)){
		printf("Couldn't find log to verify on the server\n");
		abort();
	}

	unsigned char *file;
	if((file = malloc(len))==NULL){
		printf("malloc error in t_verifylog\n");
		abort();
	}
	if(fread(file,sizeof(char),len,fp) != len){
		printf("failed to read in entire log in t_verifylog\n");
		abort();
	}

	if(!log_verifyhash(file, len, logs[log_num].A))
		return 0;
	
	FILE *ofile = fopen(out, "wb");
	if(!ofile){
		printf("couldn't open %s for writing.  Trix are for kids!\n", out);
		abort();
	}
	//inspired by strtok, which probably is a bad thing ...
	unsigned char *entry;
	size_t size = log_getmsg(file, len, logs[log_num].A, &entry, NULL, NULL);
	while(entry != NULL){
		//don't write out the trailing \0 that seems to show up
		fwrite(entry, sizeof(char), size-1, ofile);
		fprintf(ofile, "\n");
		free(entry);
		size = log_getmsg(NULL, len, NULL, &entry, NULL, NULL);
	}
	fclose(ofile);
	free(file);
	return 1;
}

/*
void t_closelog(void){
	free(logs[num].log_id);
	logs[num].log_id = NULL;
}
*/

void t_respond_ok(unsigned char *x0, size_t x0_size){
	//form x1: need the hash of x0
	unsigned char *d = NULL;   //allocated by digest_message
	size_t d_len;
	digest_message(x0, x0_size, &d, &d_len);
	if(d_len != HASH_LEN){
		printf("something went awry while hashing x0 for x1 by trusted\n");
		abort();
	}

	#if DEBUG
	FILE *ftp = fopen("t_x0.txt", "wb");
	fwrite(x0, sizeof(char), x0_size, ftp);
	fclose(ftp);
	ftp = fopen("t_hx0.txt","wb");
	fwrite(d, sizeof(char), d_len, ftp);
	fclose(ftp);
	#endif

	//increment p
	logs[num].p += 1;
	
	//allocate and fill x1 : different enough that the make_x function doesn't work darn it
	size_t x1_size = sizeof(int) + (strlen(logs[num].log_id) + 1) + HASH_LEN;
	int x1_components = 3;
	size_t x1_sizes[x1_components];
	x1_sizes[0] = sizeof(int);
	x1_sizes[1] = strlen(logs[num].log_id)+1;
	x1_sizes[2] = HASH_LEN;

	unsigned char *x1 = malloc(x1_size);
	unsigned char *tmp;
	memcpy(x1, &logs[num].p, sizeof(int));
	tmp = x1 + sizeof(int);
	memcpy(tmp, logs[num].log_id, strlen(logs[num].log_id)+1);
	tmp += strlen(logs[num].log_id) + 1;
	memcpy(tmp, d, HASH_LEN);

	//sign x1
	size_t sig_len = 0;
	unsigned char *sig = sign("t_private.pem", x1, x1_size, &sig_len);

	#if DEBUG
	if(verify("t_public.pem", x1, x1_size, sig, sig_len)!=1)
		printf("trusted can't verify his own x1 signature!\n");
	#endif

	//concatentate x1 with signature:
	int concat_components = 2;
	size_t concat_sizes[concat_components];
	concat_sizes[0] = x1_size;
	concat_sizes[1] = sig_len;
	size_t concat_size = x1_size + sig_len;
	unsigned char *concat = make_concat(x1, sig, concat_size, concat_sizes);
	
	free(x1);
	free(sig);

	//generate logs[num].tkey aka k1:
	RAND_bytes(logs[num].tkey, KEY_LEN);
	RAND_bytes(logs[num].t_iv, KEY_LEN);

	//ciphertext len with be a multiple of block size: 16 bytes = 128 bits.  Round up.
	//this math adds an extra byte, but what the hell
	unsigned char *ciphertext = malloc((concat_size + 16) & ~15);    
	size_t ciphertext_size = encrypt_aes(concat, concat_size, logs[num].tkey, logs[num].t_iv, ciphertext);
	free(concat);

	//sign the key with u_public:
	size_t encryp_key_size = 0;
	unsigned char *encryp_key = encrypt_rsa("u_public.pem", logs[num].tkey, KEY_LEN, &encryp_key_size);

	//form and send M1:
	int m1_components = 4;
	size_t m1_sizes[m1_components];
	m1_sizes[0] = sizeof(int);
	m1_sizes[1] = strlen(logs[num].log_id)+1;
	m1_sizes[2] = encryp_key_size;
	m1_sizes[3] = ciphertext_size;
	size_t m1_size = 0;
	for(int i = 0; i < m1_components; i++) m1_size += m1_sizes[i];

	unsigned char *m1 = make_m(encryp_key, ciphertext, m1_sizes, m1_size, logs[num].p, logs[num].log_id);

	dprintf("Made m1\n");

	free(encryp_key);
	free(ciphertext);
    
    //increment p to next value you'd expect in a reply
    logs[num].p += 1;

	//send m1 to U 
	u_logallowed(m1_components, m1_size, m1_sizes, m1,
				  concat_components, concat_size, concat_sizes,
				  x1_components, x1_size, x1_sizes, logs[num].t_iv);

    dprintf("Trusted finished\n");
}

/*Receives a message with num components,
 * the lengths (in bytes) of which are in the 
 * len array.  Message is in msg.
 *
 * Performs the section 3.2 functions of the 
 * trusted server.
 */
void t_allowlog(int m0_components, size_t m0_size, size_t *m0_sizes, unsigned char *m0,
				int concat_components, size_t concat_size, size_t *concat_sizes,
				int x0_components, size_t x0_size, size_t *x0_sizes, unsigned char *iv){
    
    unsigned char *sig = NULL;
   	
		//add a new entry in the array of logs we are keeping track of
		num++;
		logs[num].p = 0;
		logs[num].log_id = NULL;

    //get x0, verifying p, logs[num].log_id in the process
    unsigned char *x0 = process_message(&sig, m0_size, m0_sizes, m0, concat_size, concat_sizes, x0_size,
                                        iv, &(logs[num].log_id), logs[num].p, "t_private.pem", logs[num].ukey);
    //store the iv as well:
    memcpy(logs[num].u_iv, iv, KEY_LEN);
    
	//unpack x0
	int p_tmp;
	time_t d, d_tmp;
	unsigned char *cert = malloc(x0_sizes[2]);

	memcpy(&p_tmp, x0, x0_sizes[0]);
	unsigned char *tmp = x0 + x0_sizes[0];
	if(p_tmp != logs[num].p){
		printf("got two different p values, sanity check failure!\n");
		return;
	}
	memcpy(&d, tmp, x0_sizes[1]);
	tmp += x0_sizes[1];
	time(&d_tmp);
	if(d < d_tmp - TIMEOUT){
		printf("x0 is too old, go away\n");
		return;
	}
	memcpy(cert, tmp, x0_sizes[2]);
	tmp += x0_sizes[2];
	FILE *fp = fopen("tmp.crt", "wb");
	if(!fp) {perror("couldn't open a file\n"); abort();}
	//don't write the null terminator I added to the cert
	fwrite(cert, sizeof(char), x0_sizes[2]-1, fp);
	fclose(fp);

	memcpy(logs[num].A, tmp, x0_sizes[3]);

	//verify the certificate
	X509 *ca_cert = read_x509("t_cert.crt");
	X509 *u_cert = read_x509("tmp.crt");
	
	X509_STORE *store = X509_STORE_new();
	if(!store) handleErrors();
	if(X509_STORE_add_cert(store, ca_cert) <= 0) handleErrors();

	X509_STORE_CTX *storeCtx = X509_STORE_CTX_new();
	if(storeCtx == NULL) handleErrors();
	if(X509_STORE_CTX_init(storeCtx, store, u_cert, NULL) <= 0) handleErrors();

	if(X509_verify_cert(storeCtx) <= 0) {
		printf("failed to verify the certificate in M0!\n");
		abort();
	}
	X509_STORE_CTX_free(storeCtx);
	X509_STORE_free(store);
	
	free(ca_cert);

	//use the pub key from the certificate to verify the signature
	//inefficient, but it is the way my code is set up
	FILE *p = fopen("tmp.pem", "w");
	if(!p){ perror("fopen failed:"); abort();}

	EVP_PKEY *key = X509_get_pubkey(u_cert);
	PEM_write_PUBKEY(p, key);
	free(u_cert);
	fclose(p);
	if(verify("tmp.pem", x0, x0_size, sig, concat_sizes[1]) != 1){
		printf("failed to verify signature on x0\n");
		abort();
	}
	free(sig);

	dprintf("trusted verified m0 from untrusted, huzzah!\n");

	//form the response and send it
	t_respond_ok(x0, x0_size);	

	free(x0);
	free(m0);
}
