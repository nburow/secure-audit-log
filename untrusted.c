#include "sal.h"

//symetric key used when sending to trusted
static unsigned char ukey[KEY_LEN];

//symetric key trusted uses when talking to you
static unsigned char tkey[KEY_LEN];

static unsigned char A[HASH_LEN];
static unsigned char *log_id = NULL;
static unsigned char u_iv[KEY_LEN];
static unsigned char t_iv[KEY_LEN];

static int p = 0;
static time_t d_plus;

static unsigned char *h_x0 = NULL;
static size_t h_x0_len;

static FILE *lfp;

void u_logallowed(int m1_components, size_t m1_size, size_t *m1_sizes, unsigned char *m1,
				int concat_components, size_t concat_size, size_t *concat_sizes,
				int x1_components, size_t x1_size, size_t *x1_sizes,
				unsigned char *iv) {

	dprintf("about to verify m1 from trusted\n");

	unsigned char *sig = NULL;
	
	//get x1, verifying p, log_id in the process
	unsigned char *x1 = process_message(&sig, m1_size, m1_sizes, m1, concat_size, 
																			concat_sizes, x1_size,
																			iv, &log_id, p, "u_private.pem", tkey);
	//store the iv as well:
	memcpy(t_iv, iv, KEY_LEN);

	//verify the signature
	if(verify("t_public.pem", x1, x1_size, sig, concat_sizes[1]) != 1){
		printf("failed to verify signature on x1!\n");
		abort();
	}
	

	unsigned char *tmp;

	//verify p from x1
	int r_p;
	memcpy(&r_p, x1, x1_sizes[0]);
	tmp = x1 + x1_sizes[0];
	if(r_p != p){
		printf("p: %d from x1 does not match expected p %d!\n", r_p, p);
		abort();
	}
	
	//verify the log name from x1
	if(strncmp((const char *)tmp, (const char *)log_id, x1_sizes[1])){
		printf("log_id from x1 failed to verify!\n");
		abort();
	}
	tmp += x1_sizes[1];

	//verify hash of x0
	if(h_x0_len != x1_sizes[2] || memcmp(tmp, h_x0, x1_sizes[2])){
		printf("stored hash does not match hash from x1\n");
		printf("%d =? %d\n", h_x0_len, x1_sizes[2]);
		abort();
	}
	free(h_x0);
	h_x0 = NULL;

	time_t cur;
	time(&cur);
	if(cur >= d_plus){
		log_message(lfp, "AbnormalCloseType", (unsigned char *)cur, sizeof(time_t), NULL);
		printf("timed out you slob!\n");
	}
	log_message(lfp, "ResponseMessageType", m1, m1_size, NULL);

	p += 1;
	dprintf("communications protocol with trusted server completed successfully\n");
	free(x1);
	free(m1);
}

/*Does the untrusted side of section 3.2,
 * creating a log file
 *
 * Returns 1 on success, 0 on failure */
int u_makelog(unsigned char *name, FILE *fp){
	p = 0;
	time_t d;
	
	dprintf("starting u_makelog\n");
	lfp = fp;

	/*Step 1 from section 3.2 of the paper */
	//first create the required values:
	RAND_bytes(ukey, KEY_LEN);
	time(&d);    //current time stamp
	d_plus = d + TIMEOUT;
	if(log_id == NULL) {
		log_id = malloc(strlen(name) + 1);
		strcpy(log_id, name);
	}
	else {
		fprintf(stderr, "Can't open a new log while one is alread open\n");
		abort();
	}

	unsigned char *cert;
	size_t cert_len;
	cert = read_cert(&cert_len,"u.crt" );

	RAND_bytes(A, HASH_LEN);

	/*The next section of code creates x0 */
	//x0 = p, d, cert_u, A
	int x0_components = 4;
	size_t x0_sizes[x0_components];
	x0_sizes[0] = sizeof(int);
	x0_sizes[1] = sizeof(time_t);
	x0_sizes[2] = cert_len+1;
	x0_sizes[3] = HASH_LEN;
	
	size_t x0_size;
	unsigned char *x0 = make_x(&p, &d, cert, &x0_size, &x0_sizes, cert_len, A);

	//once the cert is in x0, we don't need it individually again
	free(cert);

	/*Now we have to create M0 
	 * p, log_id, PKE_t(ukey), Eukey(x0, SIGNu(x0))*/
	
	//encrypt ukey:
	size_t encryp_key_size = 0;
	unsigned char *encryp_key = encrypt_rsa("t_public.pem", ukey, KEY_LEN, &encryp_key_size);

	//sign x0:
	size_t sig_len = 0;
	unsigned char *sig = sign("u_private.pem", x0, x0_size, &sig_len); 
	
	//concatenate X0 with signature:
	int concat_components = 2;
	size_t concat_sizes[concat_components];
	concat_sizes[0] = x0_size;
	concat_sizes[1] = sig_len;
	size_t concat_size = x0_size + sig_len;
	unsigned char *concat = make_concat(x0, sig, concat_size, concat_sizes);

	//store h(x0) to verify m1 when server replies
	digest_message(x0, x0_size, &h_x0, &h_x0_len);
	dprintf("h(x0) len = %d\n", h_x0_len);
	
	#if DEBUG
	if(verify("u_public.pem", x0, x0_size, sig, sig_len) != 1)
		dprintf("The original signature of x0 by untrusted fails to verify\n");
	else 
		dprintf("The original signature of x0 by untrusted verified\n");
	FILE *dp = fopen("u_x0.txt","wb");
	fwrite(x0, sizeof(char), x0_size, dp);
	fclose(dp);
	dp = fopen("u_sig.txt", "wb");
	fwrite(sig, sizeof(char), sig_len, dp);
	fclose(dp);
	dp = fopen("u_concat.txt", "wb");
	fwrite(concat, sizeof(char), concat_size, dp);
	fclose(dp);
	dp = fopen("u_hx0.txt", "wb");
	fwrite(h_x0, sizeof(char), h_x0_len, dp);
	fclose(dp);
	#endif

	//we are now done with x0, signature as individual components
	free(x0);
	free(sig);
	
	//encrypt the concatentation with ukey
	RAND_bytes(u_iv, KEY_LEN);

	//ciphertext len with be a multiple of block size: 16 bytes = 128 bits.  Round up.
	//this math adds an extra byte, but what the hell
	unsigned char *ciphertext = malloc((concat_size + 16) & ~15);    
	size_t ciphertext_size = encrypt_aes(concat, concat_size, ukey, u_iv, ciphertext);
	free(concat);

	//make m0:
	int m0_components = 4;
	size_t m0_sizes[m0_components];
	m0_sizes[0] = sizeof(int);
	m0_sizes[1] = strlen(log_id)+1;
	m0_sizes[2] = encryp_key_size;
	m0_sizes[3] = ciphertext_size;
	size_t m0_size = 0;
	for(int i = 0; i < m0_components; i++) m0_size += m0_sizes[i];
	unsigned char *m0 = make_m(encryp_key, ciphertext, m0_sizes, m0_size, p, log_id);

	dprintf("Made m0\n");

	free(encryp_key);
	free(ciphertext);

	//TODO: need to make a "LogfileInitializationType log entry here
	size_t foo = 2*sizeof(time_t) + strlen(log_id) + 1 + m0_size;
	unsigned char *bar = malloc(foo);
	memcpy(bar, &d, sizeof(time_t));
	unsigned char *tmp = bar + sizeof(time_t);
	memcpy(tmp, &d_plus, sizeof(time_t));
	tmp += sizeof(time_t);
	memcpy(tmp, log_id, strlen(log_id)+1);
	tmp += strlen(log_id)+1;
	memcpy(tmp, m0, m0_size);
	
	log_message(lfp, "LogfileInitizlizationType", bar, foo, A); 
	free(bar);

	//I tried to imagine what information you would need for a network 
	//protocol.  Seemed like the number of things and how big each was 
	//would be necessary.
	p += 1;
	t_allowlog(m0_components, m0_size, m0_sizes, m0,
				  concat_components, concat_size, concat_sizes,
				  x0_components, x0_size, x0_sizes, u_iv);

	//success
	return 1;
}

void u_closelog(FILE *fp){
	log_close(fp);
	free(log_id);
	log_id = NULL;
	//t_closelog();
}
