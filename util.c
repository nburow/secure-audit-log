#include "sal.h"

//reads a certificate into an OpenSSL X509 struct
X509 *read_x509(char *file){
	FILE *fp = fopen(file, "r");
	if(!fp) {printf("failed to open file %s\n", file); abort();}
	X509* cert = PEM_read_X509(fp, NULL, NULL, NULL);
	fclose(fp);
	return cert;
}

//reads a certificate in from cert_name, stores it as an unsigned char *
unsigned char *read_cert(size_t *cert_len, char *cert_name){
	//read in the certificate
	unsigned char *cert;

	//how long is it?
	FILE *fp = fopen(cert_name, "rb");
	fseek(fp, 0, SEEK_END);
	*cert_len = ftell(fp);
	rewind(fp);

	//read it in as a string b/c why not
	if((cert = malloc((*cert_len+1)*sizeof(char))) == NULL){
		printf("malloc error\n");
		abort();
	}
	if(fread(cert, sizeof(char), *cert_len, fp) != *cert_len) {
		printf("fread error\n");
		abort();
	}
	fclose(fp);
	cert[*cert_len] = '\0';

	return cert;
}

//glues x0 / x1 together, and returns metadata via x0_size, x0_sizes
unsigned char *make_x(int *p, time_t *d, unsigned char *cert, size_t *x_size, 
						size_t *x_sizes, size_t cert_len, unsigned char *A){
	*x_size = sizeof(int) + sizeof(time_t) + (cert_len+1) + HASH_LEN;
	unsigned char *x = malloc(*x_size);
	if(x == NULL){
		printf("x0 malloc error\n");
		abort();
	}
	unsigned char *tmp = x;
	memcpy(tmp, p, x_sizes[0]);
	tmp += x_sizes[0];
	memcpy(tmp, d, x_sizes[1]);
	tmp += x_sizes[1];
	memcpy(tmp, cert, x_sizes[2]);
	tmp += x_sizes[2];
	memcpy(tmp, A, x_sizes[3]);

	return x;
}

//concatenates x and its signature
unsigned char *make_concat(unsigned char *x, unsigned char *sig, size_t concat_size,
                           size_t *concat_sizes){
	unsigned char *concat = malloc(concat_size);

	if(!concat){
		printf("malloc error for X0, sig concatenation\n");
		abort();
	}
	memcpy(concat, x, concat_sizes[0]);
	unsigned char *tmp = concat + concat_sizes[0];
	memcpy(tmp, sig, concat_sizes[1]);

	return concat;
}

//stitch m together
unsigned char *make_m(unsigned char *encryp_key, unsigned char *ciphertext, size_t *m_sizes,
                      size_t m_size, int p, char *log_id){
	unsigned char *m = malloc(m_size);
	unsigned char *tmp;

	memcpy(m, &p, m_sizes[0]);
	tmp = m + m_sizes[0];
	memcpy(tmp, log_id, m_sizes[1]);
	tmp += m_sizes[1];
	memcpy(tmp, encryp_key, m_sizes[2]);
	tmp += m_sizes[2];
	memcpy(tmp, ciphertext, m_sizes[3]);

	return m;
}

/*Returns X_i, signature of X_i out of M_i */
unsigned char *process_message(unsigned char **sig_p, size_t m_size, size_t *m_sizes,
                               unsigned char *m, size_t concat_size, size_t *concat_sizes,
							   size_t x_size, unsigned char *iv, unsigned char **id, int p,
                               char *priv_key, unsigned char *key){
	//first break m up
	unsigned char *tmp;
    char *log_id = *id;
    unsigned char *sig = *sig_p;
    
	//p
    int r_p;
	memcpy(&r_p, m, m_sizes[0]);
	tmp = m +  m_sizes[0];
    if(r_p != p) {
        printf("Received %d an unexpected %d  p value in a message\n", r_p, p);
        abort();
    }
	dprintf("\n\nTrusted server\np: %d\n", r_p);
	
	//ID
    if(!log_id) {
        log_id = malloc(m_sizes[1]);
        memcpy(log_id, tmp, m_sizes[1]);
        *id = log_id;
        dprintf("got a new log_id in a message\n");
    }
    else if(strncmp(log_id, tmp, m_sizes[1])){
        printf("log_id from message doesn't match the current log!\n");
        abort();
    }
	tmp += m_sizes[1];
	dprintf("log_id: %s\n", *id);
	
	//encrypted k0
	unsigned char *encryp_key = malloc(m_sizes[2]);
	memcpy(encryp_key, tmp, m_sizes[2]);
	tmp += m_sizes[2];

	//encrypted x0 concatenation
	unsigned char *e_concat = malloc(m_sizes[3]);
	memcpy(e_concat, tmp, m_sizes[3]);
	
	//now decrypt k0 using private key
	size_t out_len;
	tmp = decrypt_rsa(priv_key, encryp_key, m_sizes[2], &out_len);
	if(out_len != KEY_LEN){
		printf("decrypting k0 failed sanity check on size!\n");
		return 0;
	}
	memcpy(key, tmp, KEY_LEN);
	
	free(tmp);
	free(encryp_key);

	//use k0 to decrypt the concatenation
	unsigned char *concat = malloc(concat_size);
	size_t c_size = decrypt_aes(e_concat, m_sizes[3], key, iv, concat);
	if(c_size != concat_size){
		printf("the x0 concatenation decrypt failed a size sanity check!\n");
		return 0;
	}
	free(e_concat);

	//unconcatenate
	unsigned char *x = malloc(concat_sizes[0]);
	memcpy(x, concat, concat_sizes[0]);
	tmp = concat + concat_sizes[0];
	
	sig = malloc(concat_sizes[1]);
	memcpy(sig, tmp, concat_sizes[1]);
    *sig_p = sig;
	
    /*
	#if DEBUG
	FILE *dp = fopen("t_x0.txt", "wb");
	fwrite(x0, sizeof(char), concat_sizes[0], dp);
	fclose(dp);
	dp = fopen("t_sig.txt", "wb");
	fwrite(sig, sizeof(char), concat_sizes[1], dp);
	fclose(dp);
	dp = fopen("t_concat.txt", "wb");
	fwrite(concat, sizeof(char), concat_size, dp);
	fclose(dp);
	#endif
     */
	
	free(concat);
    return x;

}

//read key.  Assumes that private key doesn't have a password
EVP_PKEY *read_key(int pub, char *file){
	EVP_PKEY *key = NULL;
	FILE *keyFile = fopen(file, "r");
	if(pub){
		if((key = PEM_read_PUBKEY(keyFile, NULL, NULL, NULL)) == NULL){
			handleErrors();
		}
	}
	else{
		if((key = PEM_read_PrivateKey(keyFile, NULL, NULL, NULL)) == NULL)
			handleErrors();
	}
	fclose(keyFile);
	return key;
}
