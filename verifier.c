#include "sal.h"

/*
EVP_PKEY *get_dh_key(){
	EVP_PKEY *params, *dhkey;
	EVP_PKEY_CTX *kctx;

	if(NULL == (params = EVP_PKE_new())) handleErrors();
	if(!(kctx = EVP_PKEY_CTX_new(params, NULL))) handleErrors();

	if(1 != EVP_PKEY_keygen_init(&kctx)) handleErrors();
	if(1 != EVP_PKEY_keygen(kctx, &dhkey)) handleErrors();
	
	EVP_PKEY_free(params);
	EVP_PKEY_CTX_destroy(kctx);

	return dhkey;
}
*/

int v_entry(FILE *fp, char *log_id, int e_no){
	fflush(fp);
	FILE *f = fopen(log_id, "rb");
	int len = ftell(fp);

	unsigned char *file = malloc(len);
	if(!file){
		printf("yikes malloc failed, sob\n");
		abort();
	}
	if(fread(file, sizeof(char), len, f) != len){
		printf("failed to read in the entire log for verification\n");
		abort();
	}
	//Only verifies the Y entries, not the Z because A_0 isn't passed in
	if(!log_verifyhash(file, len, NULL))
		return 0;
	
	unsigned char *msg;
	unsigned char *key;
	unsigned char *iv;
	size_t msg_len;

  //this contains the same information as M_2, I just didn't split it all
	//up then concatenate it back into the message.
	//Note; don't have to specify the log_id, because it must be the current
	//log for this command.
	//This deviates slightly in that it returns the encrypted message as
	//well as the decryption key / iv, but it made my life easier and has
	//no security implications as that encrypted message has already been trans-
	//mitted over the wire.
	int ret = t_verifyentry(e_no, file, len, &msg, &key, &iv, &msg_len);
	if(!ret) {
		free(msg);
		free(key);
		free(iv);
		return 0;
	}
	else{
		unsigned char *plain = malloc(msg_len);
		size_t plain_len = decrypt_aes(msg, msg_len, key, iv, plain);
		printf("%s\n", plain);
		free(msg);
		free(key);
		free(iv);
		free(plain);
		return 1;
	}
}
